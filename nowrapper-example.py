import sys
import os
import serial
import time

def main():
  # Main program block 
    switch = serial.Serial(port='/dev/ttyUSB0', baudrate=9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS)
    device = serial.Serial(port='/dev/ttyUSB1', baudrate=9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS)

    while True:
        switch.write(b's')
        out = ''

        while switch.in_waiting == 0:
            time.sleep(0.1)

        if switch.in_waiting > 0:
            out = switch.read(1).decode()

        if out != '':
            if len(out) == 1:    
                if '1' in out:
                    if (sys.argv[1] == 'led'):
                        device.write('1'.encode())
                    elif (sys.argv[1] == 'buzzer'):
                        device.write('2'.encode())
                    elif (sys.argv[1] == 'temp'):
                        device.write('c'.encode())

                    while device.inWaiting() < 2:
                        time.sleep(0.1)

                    out = device.read(2).decode()

                    if out != '':
                        print (out)

                elif '0' in out:
                    if (sys.argv[1] == 'led'):
                        device.write('0'.encode())
                    elif (sys.argv[1] == 'buzzer'):
                        device.write('3'.encode())
             #elif (sys.argv[1] == 'temp'): nothing to do here

if __name__ == '__main__':
 
    try:
        if (len(sys.argv) == 1) or (len(sys.argv) > 2):
            raise ValueError('\r\nError: You did not supply the right number of arguments!\r\n')
        else:
            main()
    except KeyboardInterrupt:
        pass
    finally:
        print ("\r\nbye bye\r\n")

