"""
AdventurLabs Labkit Helper Module

A python module to interface with AdventureLabs Labkit

Mainly a wrapper around pyserial to
a. auto register AdventureLabs labkit component
b. extend methods of components for simplified use
c. remove user need to write then read to read data from components
d. quickly add and test new components


requires python3 and pyserial
"""

from serial.tools import list_ports
import time
import serial

class Component(serial.Serial):
    """Base class for all AdventureLabs USB components"""

    def __init__(self, id_, type_, device):
        self.rename(id_)
        self.device = device
        self.type_ = type_
        super().__init__(self.device)

    def rename(self, id_):
        self.id_ = id_

    def __str__(self):
        return "Component %s is a %s attached to %s" % (self.id_, self.type_, self.device)

        
class Button(Component):
    """Interface for AdventureLabs Button component"""

    def __init__(self, id_, type_, device, debouncetime=200, edgetodetect='rising'):
        """Initilisation of button class"""
        super().__init__(id_, type_, device)
        self.debouncetime = debouncetime
        self.edgetodetect = edgetodetect
        self._lastpressed = 0.0
        self._firstpressed = 0.0
        self._state = 0
        self._laststate = 0

    #TODO This method is not used yet
    def debounce(self):  #this might not be needed now using edge detect
        lapsedtime = time.perf_counter() - self._lastpressed
        if int(lapsedtime * 1000) > self.debouncetime and self._state == 1:
            self._lastpressed = time.perf_counter()
            return True
        else:
            return False
        
    #TODO this method is not used yet
    def _detect_edge(self, state, edge):
        edgedetected = False
        if state > self._laststate and edge == 'rising':
            edgedetected = True;
        elif state < self._laststate and edge == 'falling':
            edgedetected = True
        return edgedetected
            
    def _get_state_raw(self):
        self.write(b's')
        #TODO: should I sleep here?
        if self.in_waiting > 0:
            return self.readline()
        else:
            return b'0\r\n' #TODO: no response from component, what to do here?

    def get_state(self):
        if self._get_state_raw().decode('utf-8').rstrip() == '1':
            self._state = 1
        else:
            self._state = 0
        return self._state


class LED(Component):
    """Interface for AdventureLabs LED component"""

    def __init__(self, id_, type_, device, brightness=255, initalstate=1):
        super().__init__(id_, type_, device)
        #TODO: check device product ID and errro if not correct
        self._state = initalstate
        self.brightness = brightness

    def set_brightness(self, brightness=255): #TODO enable PWM
        self.brightness == brightness
        if self._state == 1:
            self.on()
    
    def on(self):
        if self.brightness == 255:
            self.write(b'1')
        self._state = 1

    def off(self):
        self.write(b'0')
        self._state = 0

    def get_state(self):
        return self._state


class TempLM35(Component):

    def __init__(self, id_, type_, device):
        super().__init__(id_, type_, device)

    def readline(self, strip=True):  #TODO: find better way than blocking request
        self.write(b'c')
        while self.in_waiting < 2:
            time.sleep(0.001) #packets hopefully written every 1ms so shouldn't take long
        line = super().readline()
        if strip == True:
            return line.decode().rstrip()
        else:
            return line

    
class Buzzer(Component):

    def __init__(self, id_, type_, device):
        super().__init__(id_, type_, device)
    
    def on(self):
        self.write(b'2')
        self._state = 1

    def off(self):
        self.write(b'3')
        self._state = 0

    def get_state(self):
        return self._state


class Host:
    """USB host controlling AdventureLabs components"""

    def __init__(self):
        self.registry = []
        self.iComponents = 0
        self.components_dictionary = {
            'Button': ['btn', Button]
            #,'Temperature Sensor LM35': ['led', LED],
            #,'Temperature Sensor LM35': ['buzz', Buzzer]
            ,'Temperature Sensor LM35': ['temp', TempLM35]
        }
        self.register_components()

    def register_components(self):
        for port in list_ports.comports():
            if port.manufacturer == "AdventureLabs":
                component_def = self.components_dictionary.get(port.description, False)
                if component_def == False:
                    print("Unknown component found on port %s" % port.device)
                else:                    
                    if self.exists_in_registry(component_def[0], port.device):
                        print("Component %s already in registry on %s" % (component_def[0], port.device))
                        #TODO: more checks here eg if port.device already registered to anothe component etc?
                    else:
                        id_ = component_def[0] + str(self.count_in_registry(component_def[0]))
                        self._register(component_def[1], id_, component_def[0], port.device)

    def exists_in_registry(self, type_, device):
        exists = False
        for component in self.registry:
            if component.type_ == type_ and component.device == device:
                return True
        
        return exists
    
    def count_in_registry(self, type_='all'):
        counter = 0
        if type_ == 'all':
            counter = len(self.registry)
        else:
            for component in self.registry:
                if component.type_ == type_:
                    counter += 1
        return counter

    def fetch_component(self, id_):
        for component in self.registry:
            if component.id_ == id_:
                return component
    
    def _register(self, class_, id_, type_, device):        
        self.registry.append(class_(id_, type_, device))
        print("ID %s is registered as %s on port %s" % (id_, class_, device))

    def __str__(self):
        counter = 0
        str_ = "Host registry consists of the following components\n"
        str_ += "----Begin Registry----\n"
        for component in self.registry:
            str_ += "Index [%s] %s \n" % (str(counter), str(component))
            counter += 1
        str_ += "----End Registry----"
        return str_

    
def main():
    """ Sample main script  - user code goes here

    The proto board I'm using at the moment has
    an led, buzzer and temnp sensor on it but it
    is configured as a Temp Sensor device on the
    FTDI chip.  
    To change the actions on protoboard edit
    the dictionary in the Host __init__ method
    """
    
    host = Host()
    print(host)
    button = host.fetch_component('btn0')
    led = host.fetch_component('led0')
    buzzer = host.fetch_component('buzz0')
    temp = host.fetch_component('temp0')
    
    while True:
        if button.get_state() == 1:
            print(button.id_ + " pressed")
            if led: led.on()
            if buzzer: buzzer.on()
            if temp: print("temp is %s" % temp.readline())
        else:
            if led: led.off()
            if buzzer: buzzer.off()
        time.sleep(0.1)

        
if __name__ == '__main__': 
    try:
        main()
    except KeyboardInterrupt:
        pass
    finally:
        print ("\r\nbye bye\r\n")
